/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

#include "../src/mode.h"
#include "../src/utils.h"
#include "../src/common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void before_check_test(){
 
    // Create file with some text.
    {
        const char text[] = "This is some text in a file";
        FILE *file = fopen("file.txt", "wb");
                
        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }
        
    // Create an empty file.
    {
        FILE *file = fopen("empty.txt", "wb");

        assert(file != NULL);
        assert(fclose(file) == 0);
    }

    // Create another file with text.
    {
        FILE *file = fopen("append.txt", "wb");
        const char text[] = "This is some text in a file";

        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }

    // Create directory with subdirectory.
    {
        assert(mkdir("directory", 0700) == 0);
        assert(mkdir("directory/subdirectory", 0700) == 0);
    }

    // Create and archive.
    {
        char *files[] = { 
            "file.txt",  
            "empty.txt", 
            "directory/" 
        };
        
        assert(tar_mode_create("archive.tar", files, 3) == 0);
    }
}

void after_check_test(){

   assert(remove("archive.tar") == 0); 
   assert(remove("file.txt") == 0);
   assert(remove("empty.txt") == 0);
   assert(remove("directory/subdirectory") == 0);
   assert(remove("directory") == 0);
   assert(remove("append.txt") == 0);

}

/**
 * tar_check() with valid archive.
 */
void test_check_valid(){
    tar_archive *archive = tar_open("archive.tar", TAR_READ);

    assert(tar_check(archive) == 0);

    tar_close(archive);
}

/**
 * tar_check() with empty archive.
 */
void test_check_empty(){
    tar_archive *archive = tar_open("empty.txt", TAR_READ);

    assert(tar_check(archive) == 0);

    tar_close(archive);
}

/**
 * tar_check() with invalid archive.
 */
void test_check_invalid(){
    tar_archive *archive = tar_open("file.txt", TAR_READ);

    assert(tar_check(archive) == -1);
    assert(tar_get_error_code() != 0);
    
    assert(strncmp(tar_get_error_str(), 
        TAR_ERR_CORRUPTED_ARCHIVE_STR, 
        strlen(TAR_ERR_CORRUPTED_ARCHIVE_STR)) == 0);

    tar_close(archive);
}

void test_check(){
    before_check_test();

    test_check_valid();
    test_check_empty();
    test_check_invalid();

    after_check_test();
}
