/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

#include "../src/tar.h"
#include "../src/common.h"
#include "../src/mode.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void before_error_test(){
    
    // Create file with some text.
    {
        const char text[] = "This is some text in a file";
        FILE *file = fopen("file.txt", "wb");
                
        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }
        
    // Create an empty file.
    {
        FILE *file = fopen("empty.txt", "wb");

        assert(file != NULL);
        assert(fclose(file) == 0);
    }

    // Create another file with text.
    {
        FILE *file = fopen("append.txt", "wb");
        const char text[] = "This is some text in a file";

        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }

    // Create directory with a sub directory.
    {
        mkdir("directory", 0700);
        mkdir("directory/subDirectory", 0700);
    }

    // Create and archive.
    {
        char *files[] = { 
            "file.txt",  
            "empty.txt", 
            "directory/" 
        };
        
        assert(tar_mode_create("archive.tar", files, 3) == 0);
    }
}

void after_error_test(){

   assert(remove("archive.tar") == 0); 
   assert(remove("file.txt") == 0);
   assert(remove("empty.txt") == 0);
   assert(remove("directory/subDirectory") == 0);
   assert(remove("directory") == 0);
   assert(remove("append.txt") == 0);

}

/**
 * tar_open()
 */
void test_open_error(){
    tar_archive *archive = tar_open("missing.tar", TAR_READ);

    assert(archive == NULL);
    assert(tar_get_error_code() != 0);

    assert(strncmp(tar_get_error_str(), 
        "No such file or directory 'missing.tar'", 
        strlen(tar_get_error_str())) == 0);
}

/**
 * tar_list()
 */
void test_list_error(){
    char *files[] = {
        "missing.txt",
    };

    tar_archive *archive = tar_open("archive.tar", TAR_READ);

    assert(tar_list(archive, files, 1) == -1);
    assert(tar_get_error_code() != 0);

    assert(strncmp(tar_get_error_str(), 
        TAR_ERR_FIND_ARCHIVE_STR,  
        strlen(TAR_ERR_FIND_ARCHIVE_STR)) == 0);

    tar_close(archive);
}

/**
 * tar_extract()
 */
void test_extract_error(){
    char *files[] = {
        "missing.txt",
    };

    tar_archive *archive = tar_open("archive.tar", TAR_READ);

    assert(tar_extract(archive, files, 1) == -1);
    assert(tar_get_error_code() != 0);

    assert(strncmp(tar_get_error_str(),  
        TAR_ERR_FIND_ARCHIVE_STR, 
        strlen(TAR_ERR_FIND_ARCHIVE_STR)) == 0);

    tar_close(archive);
}

/**
 * tar_append()
 */
void test_append_error(){
    char *files[] = {
        "missing.txt",
    };

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    
    assert(tar_append(archive, files, 1) == -1);
    assert(tar_get_error_code() != 0);

    assert(strncmp(tar_get_error_str(), 
        TAR_ERR_FILE_NOT_FOUND_STR, 
        strlen(TAR_ERR_FILE_NOT_FOUND_STR)) == 0);

    tar_close(archive);
}

/**
 * tar_delete()
 */
void test_delete_error(){
    char *files[] = {
        "missing.txt",
    };

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    
    assert(tar_delete(archive, files, 1) == -1);
    assert(tar_get_error_code() != 0);

    assert(strncmp(tar_get_error_str(), 
        TAR_ERR_FIND_ARCHIVE_STR, 
        strlen(TAR_ERR_FIND_ARCHIVE_STR)) == 0);

    tar_close(archive);
}

void test_error() {
    before_error_test();

    test_open_error();
    test_list_error();
    test_extract_error();
    test_append_error();
    test_delete_error();

    after_error_test();
}