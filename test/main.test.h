
#ifndef MAIN_TEST_H
#define MAIN_TEST_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

void test_common();
void test_mode();
void test_error();
void test_check();

#endif