/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <assert.h>
#include <string.h>
#include <stdio.h>
#include <sys/stat.h>

#include "../src/mode.h"
#include "../src/utils.h"
#include "../src/common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void before_mode_test(){
    
    // Create file with some text.
    {
        const char text[] = "This is some text in a file";
        FILE *file = fopen("file.txt", "wb");
                
        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }
        
    // Create an empty file.
    {
        FILE *file = fopen("empty.txt", "wb");

        assert(file != NULL);
        assert(fclose(file) == 0);
    }

    // Create another file with text.
    {
        FILE *file = fopen("append.txt", "wb");
        const char text[] = "This is some text in a file";

        assert(file != NULL);
        assert(fwrite(text, sizeof(char), strlen(text), file) != 0);
        assert(fclose(file) == 0);
    }

    // Create directory with sub directory.
    {
        mkdir("directory", 0700);
        mkdir("directory/subDirectory", 0700);
    }

    // Create and archive.
    {
        char *files[] = { 
            "file.txt",  
            "empty.txt", 
            "directory/" 
        };
        
        assert(tar_mode_create("archive.tar", files, 3) == 0);
    }
}

void after_mode_test(){

   assert(remove("archive.tar") == 0); 
   assert(remove("file.txt") == 0);
   assert(remove("empty.txt") == 0);
   assert(remove("directory/subDirectory") == 0);
   assert(remove("directory") == 0);
   assert(remove("append.txt") == 0);

}

/**
 * tar_mode_create()
 */
void test_create(){
    char *files[] = { 
        "file.txt",
        "empty.txt",
        "directory/" 
    };

    assert(tar_mode_create("archive.tar", files, 3) == 0);
    
    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    tar_header header;

    assert(tar_find_files(archive, files, 3) == 0);

    tar_close(archive);
}

/**
 * tar_mode_create()
 */
void test_list(){
    char *files[] = { 
        "file.txt",  
        "empty.txt", 
        "directory/" 
    };

    assert(tar_mode_create("archive.tar", files, 3) == 0);

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    tar_header header;
    
    assert(tar_rewind(archive) == 0);
    assert(tar_next(archive) == 0);
    assert(tar_read_header(archive, &header) == 0);
    assert(strcmp(header.name, files[0]) == 0);

    assert(tar_next(archive) == 0);
    assert(tar_read_header(archive, &header) == 0);
    assert(strcmp(header.name, files[1]) == 0);

    assert(tar_next(archive) == 0);
    assert(tar_read_header(archive, &header) == 0);
    assert(strcmp(header.name, files[2]) == 0);

    tar_close(archive);
}

/**
 * tar_mode_append()
 */
void test_append(){
    char *files[] = {
        "append.txt"
    };

    assert(tar_mode_append("archive.tar", files, 1) == 0);

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    tar_header header;

    assert(tar_find_file(archive, files[0]) == 0);

    tar_close(archive);
}

/**
 * tar_mode_extract()
 */
void test_extract(){
    char *files[] = {
        "append.txt"
    };
        
    assert(tar_mode_extract("archive.tar", files, 1) == 0);

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    tar_header header;

    assert(remove(files[0]) == 0);
    assert(tar_extract(archive, files, 1) == 0);
    assert(file_exists(files[0]) == 1);

    tar_close(archive);
}

/**
 * tar_mode_delete()
 */
void test_delete(){
    char *files[] = {
        "append.txt"
    };  

    assert(tar_mode_delete("archive.tar", files, 1) == 0);

    tar_archive *archive = tar_open("archive.tar", TAR_READ);
    tar_header header;

    assert(tar_find_file(archive, files[0]) == -1);

    tar_close(archive); 
}

void test_mode() {
    before_mode_test();

    test_create();
    test_list();
    test_append();
    test_extract();
    test_delete();

    after_mode_test();
}
