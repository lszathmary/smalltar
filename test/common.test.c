/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "../src/tar.h"
#include "../src/common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void before_test(){
    FILE *file = fopen("test.tar", "wb");

    assert(file != NULL);
    assert(fclose);
}

void after_test(){

    assert(remove("test.tar") == 0);
}

/**
 * tar_write()
 */
void test_tar_write(void) {
    tar_archive *archive = tar_open("test.tar", TAR_CREATE);

    char buffer[512];

    assert(tar_write(archive, buffer, sizeof(buffer)) == 0);

    assert(tar_tell(archive) == 512);
    assert(archive->position == 512);

    tar_close(archive);
}

/**
 * tar_read()
 */ 
void test_tar_read(void) {
    tar_archive *archive = tar_open("test.tar", TAR_READ);

    char buffer[512];

    assert(tar_read(archive, buffer, sizeof(buffer)) == 0);
    
    assert(tar_tell(archive) == 512);
    assert(archive->position == 512);

    tar_close(archive);
}

/**
 * tar_seek()
 */
void test_tar_seek() {
    tar_archive *archive = tar_open("test.tar", TAR_READ);

    assert(tar_seek(archive, 512) == 0);
    
    assert(tar_tell(archive) == 512);
    assert(archive->position == 512);

    tar_close(archive);
}

/**
 * tar_rewind()
 */
void test_tar_rewind() {
    tar_archive *archive = tar_open("test.tar", TAR_READ);
    tar_seek(archive, 512);

    assert(tar_rewind(archive) == 0);

    assert(tar_tell(archive) == 0);
    assert(archive->position == 0);
    assert(archive->entry_start == 0);

    tar_close(archive);
}

/**
 * tar_read_header()
 */ 
void test_tar_read_header() {
    tar_archive *archive = tar_open("test.tar", TAR_CREATE);
    tar_header header;

    // Prepare header used for test.
    {
        snprintf(header.name,  sizeof(header.name),  "%s", "foo.txt");
        int_to_octal_str(1024, header.size, sizeof(header.mode));

        tar_write(archive, &header, sizeof(tar_header));
        tar_rewind(archive);
    }

    assert(tar_read_header(archive, &header) == 0);
    assert(archive->position == 0);
    assert(archive->entry_start == 0);
    assert(strncmp(header.name, "foo.txt", 100) == 0);

    tar_close(archive);
}

void test_common() {
    before_test();

    test_tar_write();
    test_tar_read();
    test_tar_seek();
    test_tar_rewind();
    test_tar_read_header();

    after_test();
}
