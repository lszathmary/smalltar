# SmallTar

• SmTar is a very simple (around 2000 lines) implementation of GNU Tar written in C, intended for Linux-like environments (due to the use some of the POSIX library functions).

• The implementation does not focus on feature completeness or performance but, on simplicity, readability and compactness of code.

• At the moment, it provides the ability to create tar archives, extract files from previously created archives, to append additional files, to delete or to list files which were already stored. 