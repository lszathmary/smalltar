#!/bin/bash

cd test

gcc -o test        \
    ../src/common.c     \
    ../src/open.c       \
    ../src/close.c      \
    ../src/utils.c      \
    ../src/error.c      \
    ../src/dev.c        \
    ../src/list.c       \
    ../src/extract.c    \
    ../src/append.c     \
    ../src/mode.c       \
    ../src/delete.c     \
    ../src/find.c       \
    ../src/check.c      \
    mode.test.c         \
    common.test.c       \
    error.test.c        \
    check.test.c        \
    main.test.c -lm     \
    
mv test ../build/
