#!/bin/bash

cd src/

gcc -o smtar  \
    common.c    \
    open.c      \
    close.c     \
    utils.c     \
    error.c     \
    dev.c       \
    list.c      \
    extract.c   \
    append.c    \
    delete.c    \
    find.c      \
    check.c     \
    mode.c      \
    main.c -lm

mv smtar ../build