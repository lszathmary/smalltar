/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Copy data from the archive to specfied file.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_copy_data_to_file(tar_archive *archive, unsigned int size, FILE *file) {
    char buffer[512];

    while (size) {
        int read_err  = 0;
        int read_size = MIN(size, sizeof(buffer));

        if (read_err = tar_read(archive, buffer, read_size)){
            return read_err;
        }

        if (fwrite(buffer, 1, read_size, file) != read_size){
            return -1;
        }

        size -= read_size;
    }

    return 0;
}

int tar_extract_file(tar_archive *archive, tar_header *header) {
    FILE *file = fopen(header->name, "wb");

    if (file == NULL) {
        tar_set_file_error(tar_system_errno(), header->name,
                           tar_system_strerror());

        return -1;
    }

    // Skip over the header.
    if (tar_seek(archive, archive->entry_start + sizeof(tar_header))) {
        fclose(file);
        return -1;
    }

    if (tar_copy_data_to_file(archive, oct2uint(header->size), file)) {
        fclose(file);
        return -1;
    }

    fclose(file);
    return 0;
}

int tar_prepare_and_extract_file(tar_archive *archive, tar_header *header){
    int err = mkpath_default_mode(header->name);

    if (err) {
        tar_set_system_error();
        return err;
    }

    return tar_extract_file(archive, header);
}

int tar_extract_directory(tar_archive *archive, tar_header *header) {
    int mode = oct2uint(header->mode);
    int err  = mkpath(header->name, mode & 0777);

    if (err) {
        tar_set_system_error();
        return -1;
    }

    return 0;
}

int tar_extract_entry(tar_archive *archive) {
    tar_header header;
    int res = tar_read_header(archive, &header);

    if (res) {
        return res;
    }
        
    if (tar_checksum(&header) != oct2uint(header.chksum)){
        return -1;
    }

    if (header.typeflag == TAR_REGULAR) {
        return tar_prepare_and_extract_file(archive, &header);
    }
    else if (header.typeflag == TAR_DIRECTORY) {
        return tar_extract_directory(archive, &header);
    }

    return 0;
}

/**
 * Extracts the contents of the specified archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_find_and_extract_all(tar_archive *archive) {
    int err = tar_rewind(archive);

    if (err) {
        return err;
    }

    for (;;) {

        int res = tar_next(archive);
        
        // In case there are no more entries in the archive
        // we wil stop here.
        if (res) {
            return tar_result(res);
        }

        int err = tar_extract_entry(archive);
                
        if (err) {
            return tar_result(err);
        }
    }

}

int tar_find_and_extract_file(tar_archive *archive, char *path){
    int err;

    if (err = tar_find_file(archive, path)) {
        return err;
    }

    if (err = tar_extract_entry(archive)) {
        return err;
    }

    return 0;
}

int tar_extract_all(tar_archive *archive){
    int err = tar_find_and_extract_all(archive);

    if (err){
        tar_assure_error(TAR_ERR_EXTRACT_ARCHIVE,
                         TAR_ERR_EXTRACT_ARCHIVE_STR);

        return err;
    }

    return 0;
}

int tar_extract_files(tar_archive *archive, char *files[], int file_count){
    
    for (int idx = 0; idx < file_count; idx++){
        int err = tar_find_and_extract_file(archive, files[idx]);

        if (err){
            tar_assure_file_error(TAR_ERR_EXTRACT_ARCHIVE, files[idx],
                                  TAR_ERR_EXTRACT_ARCHIVE_STR);
                                  
            return err;
        }
    }

    return 0;
}

/**
 * Extract the specified list of files/directories from the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_extract(tar_archive *archive, char *files[], int file_count){

    if (file_count){
        return tar_extract_files(archive, files, file_count);
    }
    else {
        return tar_extract_all(archive);
    }

    return 0;
}
