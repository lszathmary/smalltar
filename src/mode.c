/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "mode.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * List the content of an archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_mode_list(char* archive_name, char *files[], int file_count) {
    tar_archive *archive = tar_open(archive_name, TAR_READ);

    if (archive == NULL) {
        tar_print_error();

        return -1;
    }
    
    if (tar_check(archive)){
        tar_print_error();
        return -1;
    }
    
    if (tar_list(archive, files, file_count)) {
        tar_print_error();
        tar_close(archive);

        return -1;
    }

    if (tar_close(archive)) {
        tar_print_error();

        return -1;
    }

    return 0;
}

/**
 * Extract files from an archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_mode_extract(char *archive_name, char *files[], int file_count) {
    tar_archive *archive = tar_open(archive_name, TAR_READ);

    if (archive == NULL) {
        tar_print_error();

        return -1;
    }
    
    if (tar_check(archive)){
        tar_print_error();
        return -1;
    }

    if (tar_extract(archive, files, file_count)) {
        tar_print_error();
        tar_close(archive);

        return -1;
    }

    if (tar_close(archive)) {
        tar_print_error();

        return -1;
    }

    return 0;
}

/**
 * Append the specified list of files/directories to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_mode_append(char *archive_name, char *files[], int file_count) {
    tar_archive *archive = tar_open(archive_name, TAR_UPDATE);

    if (archive == NULL) {
        tar_print_error();

        return -1;
    }

    if (tar_check(archive)){
        tar_print_error();
        return -1;
    }

    if(tar_append(archive, files, file_count)) {
        tar_print_error();
        tar_close(archive);

        return -1;
    }

    if (tar_close(archive)) {
        tar_print_error();

        return -1;
    }

    return 0;
}

/**
 * 
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_mode_create(char *archive_name, char **files, int file_count) {
    tar_archive *archive = tar_open(archive_name, TAR_CREATE);

    if (archive == NULL) {
        tar_print_error();
        return -1;
    }

    if (tar_check(archive)){
        tar_print_error();
        return -1;
    }

    if(tar_append(archive, files, file_count)) {
        tar_print_error();
        tar_close(archive);

        return -1;
    }

    if (tar_close(archive)) {
        tar_print_error();

        return -1;
    }

    return 0;
}


/**
 * 
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_mode_delete(char *archive_name, char **files, int file_count) {
    tar_archive *archive = tar_open(archive_name, TAR_UPDATE);

    if (archive == NULL) {
        tar_print_error();
        return -1;
    }

    if (tar_check(archive)){
        tar_print_error();
        return -1;
    }


    if(tar_delete(archive, files, file_count)) {
        tar_print_error();
        tar_close(archive);

        return -1;
    }

    if (tar_close(archive)) {
        tar_print_error();

        return -1;
    }

    return 0;
}
