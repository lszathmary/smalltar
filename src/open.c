/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"

#include <string.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Check the length of the path.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_chk_file_name(tar_archive *archive, const char* path) {
    size_t max_length = sizeof(archive->name) - 1;
    size_t length = (size_t) strlen(path);

    if (length > max_length){
        tar_set_file_error(TAR_ERR_PATH_TOO_LONG, path,
                           TAR_ERR_PATH_TOO_LONG_STR);
        
        return -1;
    }

    return 0;
}

/**
 * Copy file path to structure.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_set_file_name(tar_archive *archive, const char *path) {
    int len = snprintf(archive->name,  sizeof(archive->name),  "%s", path);

    if ((len < 0) && (len < sizeof(archive->name))) {
        return -1;
    }

    return 0;
}

/**
 * Open file and stream and copy pointer to structure.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_set_file_stream(tar_archive *archive, int mode) {

    switch (mode)
    {
        case TAR_CREATE:
            archive->file = fopen(archive->name, "wb+");    
            break;
            
        case TAR_READ:
            archive->file = fopen(archive->name, "rb");
            break;

        case TAR_UPDATE:
            archive->file = fopen(archive->name, "rb+");
            break;
    }

    if (archive->file == NULL){
        tar_set_file_error(tar_system_errno(), archive->name,
                           tar_system_strerror());

        return -1;
    }

    return 0;
}

/**
 * Prepare the archive file usage.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_set_file(tar_archive *archive, const char *path, int mode) {
    int err;

    if (err = tar_chk_file_name(archive, path)){
        return err;
    }

    if (err = tar_set_file_name(archive, path)){
        return err;
    }

    if (err = tar_set_file_stream(archive, mode)) {
        return err;
    }
    
    return 0;
}

/**
 * Open the specified archive.
 * 
 * Returns: if the archive is opened succesfully, the function returns a pointer
 * to tar_archive object, that can be used to identify the archive and use it 
 * for future operations. In case of error NULL is returned.
 */
tar_archive *tar_open(const char *path, int mode){
    tar_clear_error();

    tar_archive *archive = calloc(1, sizeof(tar_archive));
        
    if (archive == NULL) {
        tar_set_system_error();
        return NULL;
    }
    
    if (tar_set_file(archive, path, mode)) {
        tar_assure_file_error(TAR_ERR_OPEN_ARCHIVE, path,
                              TAR_ERR_OPEN_ARCHIVE_STR);

        tar_close(archive);
        return NULL;
    }

    return archive;
}
