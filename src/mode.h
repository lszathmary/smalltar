
#ifndef MODE_H
#define MODE_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"

/*//////////////////////////////////////////////////////////////////////////////////////
 * interfaces
 */

int tar_mode_list(char *archive_name, char *files[], int file_count);

int tar_mode_extract(char *archive_name, char *files[], int file_count);

int tar_mode_append(char *archive_name, char *files[], int file_count);

int tar_mode_create(char *archive_name, char *files[], int file_count);

int tar_mode_delete(char *archive_name, char **files, int file_count);

#endif