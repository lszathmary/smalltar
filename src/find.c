/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

int tar_seek_to_file(tar_archive *archive, const char *path){
    int err = tar_rewind(archive);

    if (err){
        return err;
    }

    for (;;){
        int res = tar_next(archive);
        
        if (res) {
            return res;
        }
       
        if (tar_match_file_name(archive, path)){
            return 0;
        }
    } 
}

/**
 * Determines whether the specified file is found in the archive.
 * 
 * Returns: 0 on success or -1 if the file is missing.
 */ 
int tar_find_file(tar_archive *archive, const char *path){
    int res = tar_seek_to_file(archive, path);

    if (res){
        tar_set_file_error(TAR_ERR_FIND_ARCHIVE, path,
                           TAR_ERR_FIND_ARCHIVE_STR);

        return -1;
    }

    return 0;
}

/**
 * Determines whether the specified files are found in the archive. 
 * 
 * Returns: 0 on success or -1 if at least one file is missing.
 */ 
int tar_find_files(tar_archive *archive, char *files[], int file_count){
    for (int idx = 0; idx < file_count; idx++) {

        int err = tar_find_file(archive,files[idx]);
        
        if (err) {
            return err;
        }
    }

    return 0;
}