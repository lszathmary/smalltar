/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */ 

unsigned int oct2uint(char *oct) {
    unsigned int out = 0;
    sscanf(oct, "%o", &out);
    return out;
}

int int_to_str(int value, char *str, size_t str_length) {
    int len = snprintf(str, str_length,  "%.*d", (int) (str_length - 1), value);

    if ((len < 0) && (len < sizeof(str_length))) {
        return -1;
    }

    return 0;
}

int int_to_octal_str(int value, char *octal, size_t octal_length) {
    int len = snprintf(octal, octal_length, "%.*o", (int)(octal_length-1), (unsigned) value);

    if ((len < 0) && (len < octal_length)) {
        return -1;
    }

    return 0;
}

long fsize(FILE * file) {
    long size;
    int err;
    
    if ((err = fseek(file, 0L, SEEK_END)) != 0) {
        return -1;
    }

	if ((size = ftell(file)) == -1) {
        return -1;
    }

    if ((err = fseek(file, 0L, SEEK_SET)) != 0) {
        return -1;
    }

    return size;
}

long round_up(long value, long divider) {
    return ceil((double) value / (double) divider) * divider;
}

int is_zeroed(const char* ptr, size_t size) {

    // verify this
    for (int idx = 0; idx < size; idx++) {
		if (ptr[idx] != 0){
			return 0;
		}
	}

	return 1;
}

int mkpath(const char* dir, mode_t mode){
 	struct stat sb;

    if (!dir) {
		return -1;
	}

	if (!stat(dir, &sb))
		return 0;

	mkpath(dirname(strdup(dir)), mode);

	return mkdir(dir, mode);
}

int mkpath_default_mode(const char *path){
    char *dir = dirname(strdup(path));

    return mkpath(dir, 0755);
}

int file_exists(const char *file_name) {
    struct stat st;   
  
    return (stat (file_name, &st) == 0);
}

int file_is_directory(const char *file_name){
    struct stat st;   
    
    if (stat(file_name, &st) != 0){
       return 0;
    }

    return S_ISDIR(st.st_mode);
}