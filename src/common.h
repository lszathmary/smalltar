
#ifndef TAR_COMMON_H
#define TAR_COMMON_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * macros
 */

/**
 * Determines whether the result of a function is an error.
 * 
 * Returns: 1 in case of an error, otherwise returns 0.
 */
#define tar_is_error(res)       \
        (res == -1) ? 1 : 0     


/**
 * Determines whether the result of a function is an end of file.
 * 
 * Returns: 1 in case of an eof, otherwise returns 0.
 */
#define tar_is_eof(res)         \
        (res == 1) ? 1 : 0      


/**
 * Normalize the result of the function.
 * 
 * Returns: -1 in case of an error, otherwise returns 0.
 */
#define tar_result(res)         \
        (res == 1) ? 0 : -1     

/* //////////////////////////////////////////////////////////////////////////////////////
 * interfaces
 */

int tar_write(tar_archive *archive, const void *data, unsigned int size);

int tar_read(tar_archive *archive, void *data, unsigned int size);

int tar_seek(tar_archive *archive, unsigned position);

int tar_rewind(tar_archive *archive);

long tar_tell(tar_archive *archive);

int tar_read_header(tar_archive *archive, tar_header *header);

int tar_next(tar_archive *archive);

int tar_match_file_name(tar_archive *archive, const char *file_name);

int tar_match_file_names(tar_archive *archive, char *files[], int file_count);

int tar_append_eof(tar_archive *archive);

unsigned int tar_checksum(tar_header *header);

#endif