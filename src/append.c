/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"
#include "utils.h"

#include <sys/stat.h>
#include <stddef.h>
#include <dirent.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Check the length of the path.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_check_path_length(const char *path) {
    size_t max_length = sizeof(((tar_header *)0)->name) - 1;
    size_t length = (size_t) strlen(path);

    if (length > max_length) {
        tar_set_file_error(TAR_ERR_PATH_TOO_LONG, path,
                           TAR_ERR_PATH_TOO_LONG_STR);

        return -1;
    }

    return 0;
}

/**
 * Assure that the directory name ends with a slash character.
 */
int tar_patch_directory_name(char *patched_path, const char *path){
    size_t length = strlen(path);
    
    if ((path[length - 1] != '/')) {
        sprintf(patched_path, "%s/", path);
    }
    else {
        sprintf(patched_path, "%s", path);
    }

    return 0;
}

/**
 * Check wheather file type is supported or not.
 * 
 * Returns: 1 for unsupported file types, otherwise returns 0.
 */
int tar_is_file_type_unsupported(const char *path){
    struct stat st;

    if (lstat(path, &st)) {
        return 0;
    }

    if (S_ISDIR(st.st_mode)) {
        return 0;
    }

    if (S_ISREG(st.st_mode)) {
        return 0;
    }

    return 1;
}

int tar_header_copy_path_name(tar_header *header, const char *path) {
    int len = snprintf(header->name,  sizeof(header->name),  "%s", path);

    if ((len < 0) && (len < sizeof(header->name))) {
        return -1;
    }

    return 0;
}

int tar_header_set_name(tar_header *header, struct stat *st, const char *path){
    int err; 
    
    if (err = tar_check_path_length(path)){
        return err;
    }

    if (err = tar_header_copy_path_name(header, path)){
        return err;
    }

    return 0;
}

/**
 * Set the mode field of the header for a directory.
 */
int tar_header_set_default_dir_mode(tar_header *header){
    const int dir_mode = 40777;

    if (int_to_str(dir_mode, header->mode, sizeof(header->mode))) {
        return -1;
    }

    return 0;
}

/**
 * Set the mode field of the header for a file.
 */
int tar_header_set_default_file_mode(tar_header * header){
    const int file_mode = 100777;

    if (int_to_str(file_mode, header->mode, sizeof(header->mode))) {
        return -1;
    }

    return 0;
}

int tar_header_set_default_mode(tar_header *header, struct stat *st, const char*path){

    if (S_ISREG(st->st_mode)) {
        return tar_header_set_default_file_mode(header);
    }
    
    if (S_ISDIR(st->st_mode)) {
        return tar_header_set_default_dir_mode(header);
    }

    tar_set_file_error(TAR_ERR_FILE_NOT_SUPPORTED, path,
                       TAR_ERR_FILE_NOT_SUPPORTED_STR);

    return -1;
}

int tar_header_set_time(tar_header *header, struct stat *st){
    
    if (int_to_octal_str(st->st_mtime, header->mtime, sizeof(header->mtime))){
        return -1;
    }

    return 0;
}

int tar_header_set_size(tar_header *header, struct stat *st) {
    size_t size = 0;

    if (S_ISREG(st->st_mode)) {
        size = st->st_size;
    }
    
    if (int_to_octal_str(size, header->size, sizeof(header->size))){
        return -1;
    }

    return 0;
}

int tar_header_set_default_uid(tar_header *header){
    
    if (int_to_octal_str(0, header->uid, sizeof(header->uid))){
        return -1;
    }

    return 0;
}

int tar_header_set_default_gid(tar_header *header) {
    
    if (int_to_octal_str(0, header->gid, sizeof(header->gid))){
        return -1;
    }

    return 0;
}

int tar_header_set_type(tar_header *header, struct stat *st, const char* path) {

     if (S_ISREG(st->st_mode)) {
        header->typeflag = TAR_REGULAR;
        return 0;
    }

    if (S_ISDIR(st->st_mode)) {
        header->typeflag = TAR_DIRECTORY;
        return 0;
    }

    tar_set_file_error(TAR_ERR_FILE_NOT_SUPPORTED, path,
                       TAR_ERR_FILE_NOT_SUPPORTED_STR);

    return -1;
}

int tar_header_set_checksum(tar_header *header) {
    int chksum = tar_checksum(header);

    if (int_to_octal_str(chksum, header->chksum, sizeof(header->chksum))){
        return -1;
    }

    return 0;
}

int tar_header_set(tar_header *header, const char *path){
    struct stat st;
    
    if (lstat(path, &st)) {
        tar_set_file_error(tar_system_errno(), path,
                           tar_system_strerror());

        return -1;
    }
    
    int err;

    if (err = tar_header_set_name(header, &st, path)){
        return err;
    }

    if (err = tar_header_set_default_mode(header, &st, path)){
        return err;
    }

    if (err = tar_header_set_default_gid(header)){
        return err;
    }
    
    if (err = tar_header_set_default_uid(header)){
        return err;
    }

    if (err = tar_header_set_size(header, &st)){
        return err;
    }

    if (err = tar_header_set_time(header, &st)){
        return err;
    }

    if (err = tar_header_set_type(header, &st, path)){
        return err;
    }

    // Checksum should last because, all the other values
    // have to be present in the header in order to calculate it.

    if (err = tar_header_set_checksum(header)) {
        return err;
    }

    return 0;
}

/**
 * Seek to the end the last file in the archive, before the the two empty blocks.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_seek_to_end(tar_archive *archive) {

    for (;;) {

        int res = tar_next(archive);
   
        // In case there are no more entries in the archive
        // we wil stop here.
        if (res) {
            return tar_result(res);
        }
    }

}

/**
 * Copy the content of the specified file into the archive.
 * 
 * return: If successful, the function returns zero, otherwise
 * it returns -1.
 */
int tar_copy_data_to_archive(tar_archive *archive, unsigned int file_size, FILE *file){
    char buffer[512];
    int write_err;
    int write_size;
    
    while (file_size) {
        write_size = MIN(file_size, sizeof(buffer));

        if (fread(buffer, 1, write_size, file) != write_size){
            return -1;
        }
        
        if (write_err = tar_write(archive, buffer, write_size)){
            return write_err;
        }

        file_size -= write_size;
    }

    // Each block has to 512; so will have to fill 
    // the remaining space will zeroes.
    if (write_size = (sizeof(buffer) - write_size)) {
        memset(buffer, '\0', sizeof(write_size));

        if (write_err = tar_write(archive, buffer, write_size)) {
            return write_err;
        }
    }

    return 0;
}

/**
 * Append the contents of the specified file to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_file_content(tar_archive *archive, const char *path){
    FILE *file = fopen(path, "rb");
    
    if (file == NULL) {
        tar_set_file_error(tar_system_errno(), path,
                           tar_system_strerror());

        return -1;
    }

    long size;

    if ((size = fsize(file)) == 0) {
        fclose(file);
        return 0;
    }
    
    if (tar_copy_data_to_archive(archive, size, file)) {
        fclose(file);
        return -1;
    }

    fclose(file);
    return 0;
}

int tar_append_header(tar_archive *archive, const char *path){
    tar_header header;
    memset(&header, 0, sizeof(header));  

    if (tar_header_set(&header, path)){
        return -1;
    }

    if (tar_write(archive, &header, sizeof(tar_header))) {
        return -1;
    }

    return 0;
}

/**
 * Append the content of the directory to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_directory_content(tar_archive *archive, const char* path){
    DIR *directory;
    
    if ((directory = opendir(path)) == NULL){
        tar_set_file_error(tar_system_errno(), path,
                           tar_system_strerror());

        return -1;
    }

    struct dirent *entry;

    while((entry = readdir(directory)) != NULL){
        char entry_path[1024];
        char *entry_path_ptr = entry_path;

        if (strcmp(entry->d_name, ".") == 0){
            continue;
        }

        if (strcmp(entry->d_name, "..") == 0) {
            continue;
        }
        
        // Path should be relative to parent.
        snprintf(entry_path, sizeof(entry_path), "%s%s", path, entry->d_name);

        if (tar_append(archive, &entry_path_ptr, 1)){
            closedir(directory);
            return -1;
        }
    }

    closedir(directory);
    return 0;
}

/**
 * Append the specified file to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_file(tar_archive *archive, const char *path){
    int err;

    if(err = tar_seek_to_end(archive)){
        return err;
    }

    if (err = tar_append_header(archive, path)){
        return err;
    }

    if (err = tar_append_file_content(archive, path)){
        return err;
    }

    return 0;
}

/**
 * Append the specified directory to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_directory(tar_archive *archive, const char *path){
    int  err;
    
    if (err = tar_check_path_length(path)) {
        return err;
    }

    char dir_path[100];

    if (err = tar_patch_directory_name(dir_path, path)) {
        return err;
    }

    if (err = tar_seek_to_end(archive)){
        return err;
    }

    if (err = tar_append_header(archive, dir_path)){
        return err;
    }

    if (err = tar_append_directory_content(archive, dir_path)){
        return err;
    }

    return 0;
}

/**
 * Append the specified directory or file to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_file_or_directory(tar_archive *archive, const char *path){

    if (file_is_directory(path)){

        return tar_append_directory(archive, path);
    }
    else {

        return tar_append_file(archive, path);
    }

}

/**
 * Prepare for appending specified directories or files to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_prepare_for_append(tar_archive *archive, char *files[], int file_count){
    for (int idx = 0; idx < file_count; idx++){
        char *file = files[idx];

        if (!file_exists(file)){
            tar_set_file_error(TAR_ERR_FILE_NOT_FOUND, file,
                               TAR_ERR_FILE_NOT_FOUND_STR);

            return -1;
        }
        
        if (tar_is_file_type_unsupported(file)){
            tar_set_file_error(TAR_ERR_FILE_NOT_SUPPORTED, file,
                               TAR_ERR_FILE_NOT_SUPPORTED_STR);

            return -1;
        }
    }

    return 0;
}

/**
 * Append end of file blocks after appending the files.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_finish_append(tar_archive *archive){
    
    if (tar_append_eof(archive)){
        tar_set_error(TAR_ERR_APPEND_ARCHIVE,
                      TAR_ERR_APPEND_ARCHIVE_STR);

        return -1;
    }

    return 0;
}

int tar_append_files(tar_archive *archive, char *files[], int file_count){
    for (int idx = 0; idx < file_count; idx++) {
        char *file = files[idx];

        if (tar_append_file_or_directory(archive, file)){
            tar_assure_file_error(TAR_ERR_APPEND_ARCHIVE, file,
                                  TAR_ERR_APPEND_ARCHIVE_STR);

            return -1;
        }
    }

    return 0;
}

/**
 * Append the specified list of files/directories to the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append(tar_archive *archive, char *files[], int file_count){
    int err;

    if (err = tar_prepare_for_append(archive, files, file_count)){
        return err;
    }

    if (err = tar_append_files(archive, files, file_count)){
        return err;
    }
   
    if (err = tar_finish_append(archive)){
        return err;
    }

    return 0;
}
