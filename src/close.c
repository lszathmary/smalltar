/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Close the file stream
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_close_file(tar_archive *archive) {
    
    if ((archive->file != NULL) && (fclose(archive->file))) {
        tar_set_file_error(tar_system_errno(), archive->name,
                           tar_system_strerror());

        return -1;
    }

    return 0;
}

/**
 * Close the specified archive. Even if the call fails, the stucture 
 * passed as parameter will no longer be associated with the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_close(tar_archive *archive) {
    int res = 0;

    if (archive != NULL){
        res = tar_close_file(archive);

        free(archive);
        archive = NULL;
    }
    
    return res;
}
