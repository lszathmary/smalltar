/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"

#include <stddef.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/*** 
 * Writes the number of bytes specified into the archive.
 *  
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_write(tar_archive *archive,  const void *data, unsigned int size){
    unsigned int res = fwrite((data), 1, size, archive->file);

    // Advance the position indicator.
    archive->position += res;

    if (res != size){
        return -1;
    }
    
    return 0;
}

/**
 * Reads the number of bytes specfied from the an archive.
 * 
 * Returns: 0 on success, -1 if an error occured or 1 if the
 * end of file was reached.
 */
int tar_read(tar_archive *archive, void *data, unsigned int size){
    unsigned int res = fread((data), 1, size, archive->file);

    // Advance the position indicator.    
    archive->position += res;

    // This is might be an error or just an unexpected end of file.
    if (res == 0) {
        return (ferror(archive->file)) ? -1 : 1;
    }
    
    if (res != size) {
        return -1;
    }

    return 0;
}

/**
 * Sets the position indicator for the specified archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_seek(tar_archive *archive, unsigned position){
    int err = fseek(archive->file, position, SEEK_SET);

    if (err != 0) {
        return -1;
    }

    // Update the position indicator.    
    archive->position = position;

    return 0;
}

/**
 * Sets the position indicator to the begining of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_rewind(tar_archive *archive){
    int err = fseek(archive->file, 0L, SEEK_SET);

    if (err != 0){
        return -1;
    }

    // Update the position indicator.
    archive->position = 0;
    archive->entry_start = 0;
    archive->entry_end = 0;

    return 0;
}

/**
 * Returns the current value of the position indicator for the 
 * specified archive.
 * 
 * Returns: the value of the position indicator or -1 if an error occured.
 */
long tar_tell(tar_archive *archive){
    long pos = ftell(archive->file);
    
    if (pos == -1){
        return -1;
    }

    return pos;
}

/**
 * Read a header from the current position of the archive. 
 * 
 * Returns: 0 on success, -1 if an error occured or 1 if the
 * end of file was reached.
 */
int tar_read_header(tar_archive *archive, tar_header *header){
    int res;

    // This could be an error or just a simple EOF.
    if (res = tar_read(archive, header, sizeof(tar_header))){

        // Seek to the end of the previous file.
        int err = tar_seek(archive, archive->entry_end);
        
        if (err) {
            return err;
        }
    
        return res;
    }

    // In case the header contains only zeroes we have reached the end of the archive.
    if (res = is_zeroed((char*) header, sizeof(tar_header))){

        // Seek to the end of the previous file.
        int err = tar_seek(archive, archive->entry_end);
        
        if (err) {
            return err;
        }

        return res;
    }
    
    archive->entry_start = archive->position - sizeof(tar_header);

    // Move the pointer to the begining of the header.
    return tar_seek(archive, archive->entry_start);
 }

/**
 * Move to the next file in the archive.
 * 
 * Return: Returns: 0 on success, -1 if an error occured or 1 if the
 * end of file was reached.
 */
int tar_next(tar_archive *archive){
    tar_header header;

    // Go to the end of the previous entry.
    int err = tar_seek(archive, archive->entry_end);
    
    if (err) {
        return err;
    }

    // Try reading a header.
    int res = tar_read_header(archive, &header);

    if (res) {
        return res;
    }

    // Compute the position of the end of the current entry.
    int entry_size = oct2uint(header.size);
    int entry_end  = round_up(entry_size, 512) + sizeof(header);
    
    archive->entry_end = archive->position + entry_end;

    return 0;
}

/**
 * Compares the specified file name to the one in the current entry.
 *  
 * Returns: 1 if the file name are equal, otherwise returns 0.
 */
int tar_match_file_name(tar_archive *archive, const char* file_name){
    tar_header header;
    int err = tar_read_header(archive, &header);

    if (err) {
        return 0;
    }

    return (strncmp(header.name, file_name, sizeof(header.name)) == 0);
}

/**
 * Compares the specified file names to the one in the current entry.
 *  
 * Returns: 1 if one of the file name matches with the current entry, otherwise returns 0.
 */
int tar_match_file_names(tar_archive *archive,  char* files[], int file_count){
    tar_header header;
    int err = tar_read_header(archive, &header);

    if (err) {
        return 0;
    }

    for (int idx = 0; idx < file_count; idx++) {
        if (tar_match_file_name(archive, files[idx])){
            return 1;
        }
    }

    return 0;
}

/**
 * Write the two empty block to the file; that signify the end of the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_append_eof(tar_archive *archive){
    char data[1024];
    int  err;
    
    memset(&data, '\0', sizeof(data));

    if (err = tar_write(archive, &data, sizeof(data))) {
        return err;
    }

    // Seek to the end of the last file in the archive.
    if (err = tar_seek(archive, (archive->position) - sizeof(data))) {
        return err;
    }
    
    return 0;
}

/**
 * Calculate the checksum for a tar header; counting the checksum field as all blanks.
 * 
 * Return: The computed checksum for the specified header.
 */
unsigned int tar_checksum(tar_header *header){
    unsigned char *ptr = (unsigned char*) header;
    unsigned int cheksum = 256;
   
    // The cheksum represents the simple sum of all bytes in the header block.
    // When calculating the checksum, the chksum field is treated as if it were all blanks. 

    for (int idx = 0; idx < offsetof(tar_header, chksum); idx++) {
        cheksum += ptr[idx];
    }

    for (int idx = offsetof(tar_header, typeflag); idx < sizeof(tar_header); idx++) {
        cheksum += ptr[idx];
    }

    return cheksum;
}
