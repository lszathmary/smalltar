
/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void tar_dev_print_tar(tar_archive *archive) {
    
    printf("\n");
    printf(" name:          %s\n", archive->name);
    printf(" position:      %lu\n", archive->position);
    printf(" entry_start:   %lu\n", archive->entry_start);
    printf(" entry_end:     %lu\n", archive->entry_end);
    printf("\n");

}

void tar_dev_print_header(tar_header *header) {
    
    printf("\n");
    printf(" name:     %s\n", header->name);
    printf(" mode:     %s\n", header->mode);
    printf(" uid:      %s\n", header->uid);
    printf(" gid:      %s\n", header->gid);
    printf(" size:     %s (%d)\n", header->size, oct2uint(header->size));
    printf(" mtime:    %s\n", header->mtime);
    printf(" chksum:   %s (%d)\n", header->chksum, oct2uint(header->chksum));
    printf(" typeflag: %c\n", header->typeflag);

}