
#ifndef TAR_ERROR_H
#define TAR_ERROR_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * types
 */

typedef struct {
    char filename[256];
    int  line;
    char function[256];

    int  code;
    char message[256];
} tar_error;

/* //////////////////////////////////////////////////////////////////////////////////////
 * error codes
 */

enum {
    TAR_ERR_FILE_NOT_FOUND          = 101,
    TAR_ERR_PATH_TOO_LONG           = 102,
    TAR_ERR_FILE_NOT_SUPPORTED      = 103,

    TAR_ERR_CORRUPTED_ARCHIVE       = 104,
    TAR_ERR_FIND_ARCHIVE            = 105,
    TAR_ERR_OPEN_ARCHIVE            = 106,
    TAR_ERR_DELETE_ARCHIVE          = 107,
    TAR_ERR_EXTRACT_ARCHIVE         = 108,
    TAR_ERR_LIST_ARCHIVE            = 109,
    TAR_ERR_APPEND_ARCHIVE          = 110,
};

/* //////////////////////////////////////////////////////////////////////////////////////
 * error messages
 */

#define TAR_ERR_FILE_NOT_FOUND_STR        "File not found"
#define TAR_ERR_FILE_NOT_SUPPORTED_STR    "File types is not supported"
#define TAR_ERR_PATH_TOO_LONG_STR         "File path is too long"

#define TAR_ERR_CORRUPTED_ARCHIVE_STR     "The archive seems to be corrupted"
#define TAR_ERR_OPEN_ARCHIVE_STR          "Failed to open archive"
#define TAR_ERR_FIND_ARCHIVE_STR          "Failed to find file in archive"
#define TAR_ERR_DELETE_ARCHIVE_STR        "Failed to delete file(s) from archive"
#define TAR_ERR_EXTRACT_ARCHIVE_STR       "Failed to extract file(s) from archive"
#define TAR_ERR_LIST_ARCHIVE_STR          "Failed to list file(s)"
#define TAR_ERR_APPEND_ARCHIVE_STR        "Failed to append file(s) to archive"

/* //////////////////////////////////////////////////////////////////////////////////////
 * macros
 */

/**
 * Set error with details taken from the current value of errno.
 */
#define tar_set_system_error()  \
    _tar_set_system_error (__FILE__, __LINE__, __FUNCTION__);

/**
 * Set error with specified details.
 */
#define tar_set_error(code, message)  \
    _tar_set_error(__FILE__, __LINE__, __FUNCTION__, code, message);

/**
 * Set error with specified details, only if there is no error code already set.
 */
#define tar_assure_error(code, message)  \
    _tar_assure_error(__FILE__, __LINE__, __FUNCTION__, code, message);

/**
 * Set a file related error with specified details.
 */
#define tar_set_file_error(code, message, file_name)  \
    _tar_set_file_error(__FILE__, __LINE__, __FUNCTION__, code, message, file_name);

/**
 * Set a file related error with specified details, only if there is no error code already set.
 */
#define tar_assure_file_error(code, message, file_name)  \
    _tar_assure_file_error(__FILE__, __LINE__, __FUNCTION__, code, message, file_name);

/* //////////////////////////////////////////////////////////////////////////////////////
 * interfaces
 */

void _tar_set_system_error(const char *filename, int line, const char *function);

void _tar_set_error(const char *filename, int line, const char *function, 
    int code, const char *message);

void _tar_set_file_error(const char *filename, int line, const char *function, 
    int code, const char *message, const char *file_name);

void _tar_assure_error(const char *filename, int line, const char *function,
    int code, const char *message);

void _tar_assure_file_error(const char *filename, int line, const char *function, 
    int code, const char *message, const char *file_name);

int tar_get_error_code();

char* tar_get_error_str();

void tar_print_dev_error();

void tar_clear_error();

void tar_print_error();

int tar_system_errno();

char *tar_system_strerror();

#endif