/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <argp.h>
#include <stdio.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * types
 */

typedef struct {

    // The main operation mode (ex. append, delete ...)
    char mode;

    // The tar file.
    char *archive;
    
    // The files on which to performed the operation on.
    char **files;

    // The number of files.
    int  file_count;

} tar_arguments;


/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Callback function that parsing of the options and arguments.
 */ 
static int parse_opt (int key, char *arg, struct argp_state *state) {
    tar_arguments *arguments = state->input;

    switch (key) { 
        case ARGP_KEY_INIT: {
                arguments->mode = 0;
                arguments->archive = NULL;
                arguments->file_count = 0;
                arguments->files = NULL;
            }
            break;
        case 'c': 
        case 'd':
        case 'r':
        case 't':
        case 'u':
        case 'x': {
                if (arguments->mode != 0) {
                    argp_error(state, "You may not specify more than one main operation mode");
                }
                arguments->mode = key;
            }
            break; 
        case ARGP_KEY_NO_ARGS: {
                // The archive file must be specified for the operation mode.
                if (arguments->mode != 0) {
                    argp_error(state, "You must specify the archive file");
                }
            }
            break;
        case ARGP_KEY_ARG: {
                // Parse the name of the archive file.
                if (arguments->archive == NULL) {
                    arguments->archive = arg;
                }
                else {
                    // Triggers the ARGP_KEY_ARGS key
                    return ARGP_ERR_UNKNOWN;
                }
            }
            break;
        case ARGP_KEY_ARGS: {
                // The remaining arguments are file names.
                arguments->files = state->argv + state->next;
                arguments->file_count = state->argc - state->next;
            }
            break;
        case ARGP_KEY_END: {
                // At least one operation mode must be specified.
                if (arguments->mode == 0){
                    argp_error(state, "Must specify one of -c, -d, -r, -t, -x");
                }

                // File name is mandatory for delete mode.
                if ((arguments->mode == 'd') && (arguments->file_count == 0)){
                    argp_error(state, "Must specify file name for delete mode");
                }
            }
            break;
        default:
            return ARGP_ERR_UNKNOWN;
        
    } 

    return 0; 
}

int tar_arguments_parse(tar_arguments *arguments, int argc, char **argv) {
        
    // Program name and version
    char *argp_program_version = "smtar 0.0.5";

    // Email addres for error reporting
    char *argp_program_bug_address = "levent.szathmary@gmail.com";

    // Program Description
    char doc[] = "This program is not meant to be a full tar implementation.\n"\
                 "Only a subset of the functions the GNU tar utility has are supported.";

    // Argument description
    char args_doc[] = "ARCHIVE [FILE]...";

    // Options
    struct argp_option options[] = {

        { 0, 0, 0, 0, "Main operation mode:"},

        { "create", 'c', 0, 0, "create a new archive" },
        { "delete", 'd', 0, 0, "delete from the archive"},
        { "append", 'r', 0, 0, "append file to the end of the archive"},
        { "list",   't', 0, 0, "list the content of an archive"},
        { "extract",'x', 0, 0, "extract files from an archive"},

        {0}
    };
    
    // Setup the argument parser.
    struct argp argp = { options, parse_opt, args_doc, doc }; 

    return argp_parse(&argp, argc, argv, 0, 0, arguments);
}
