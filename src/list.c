/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

int tar_list_entry(tar_archive *archive) {
    tar_header header;
    int err = tar_read_header(archive, &header);

    if (err) {
        return err;
    }

    if (tar_checksum(&header) != oct2uint(header.chksum)){
        return -1;
    }

    printf("%s\n", header.name);

    return 0;
}

/**
 * Lists the contents of the specified archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_find_and_list_all(tar_archive *archive){
    int err = tar_rewind(archive);

    if (err) {
        return err;
    }

    for (;;) {

        int res = tar_next(archive);
        
        // In case there are no more entries in the archive
        // we wil stop here.
        if (res) {
            return tar_result(res);
        }

        int err = tar_list_entry(archive);
    
        if (err) {
            return tar_result(err);
        }
    }

}

int tar_list_all(tar_archive *archive){
    int err = tar_find_and_list_all(archive);

    if (err){
        tar_set_error(TAR_ERR_LIST_ARCHIVE,
                      TAR_ERR_LIST_ARCHIVE_STR);

        return err;
    }

    return 0;
}

int tar_find_and_list_file(tar_archive *archive, char *path){
    int err;

    if (err = tar_find_file(archive, path)) {
        return err;
    }

    if (err = tar_list_entry(archive)) {
        return err;
    }

    return 0;
}

int tar_list_files(tar_archive *archive, char *files[], int file_count){

    for (int idx = 0; idx < file_count; idx++){
        int err = tar_find_and_list_file(archive, files[idx]);

        if (err) {
            tar_assure_file_error(TAR_ERR_LIST_ARCHIVE, files[idx],
                                  TAR_ERR_LIST_ARCHIVE_STR);

            return err;
        }
    }

    return 0;
}

/**
 * List the content the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_list(tar_archive *archive, char *files[], int file_count){
    
    if (file_count) {
        return tar_list_files(archive, files, file_count);
    }
    else {
        return tar_list_all(archive);
    }

    return 0;
}
