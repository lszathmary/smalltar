/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "tar.h"
#include "common.h"

#include <unistd.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Seek to the first entry that has one of the specified file name.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */ 
int tar_seek_to_first_match(tar_archive *archive, char *files[], int file_count){
    int err = tar_rewind(archive);

    if (err){
        return -1;
    }

    for (;;) {
        int res = tar_next(archive);
               
        if (res){
            return -1;
        }

        if (tar_match_file_names(archive, files, file_count)){
            return 0;
        }
    }
}

int tar_begin_delete(tar_archive *archive, char *files[], int file_count){
    int err = tar_seek_to_first_match(archive, files, file_count);
    
    if (err) {
        return err;
    }

    // Ovewrite starting from this entry.
    archive->write_position = archive->entry_start;

    return 0;
}

int tar_end_delete(tar_archive *archive){
    int err;
    
    if (err = ftruncate(fileno(archive->file), archive->write_position)){
        return err;
    }
    
    if (err = tar_seek(archive, archive->write_position)){
        return err;
    }

    if (err = tar_append_eof(archive)){
        return err;
    }
    
    archive->write_position = -1;
    return 0;
}

/**
 * Move block inside the archive, from one position to another one.
 *
 * Note: to_position has to be smaller than from_position, and size 
 * has to be divisible by 512 .
 * 
 * Return: If successful, the function returns zero, otherwise it returns -1.
 */
int tar_move_data(tar_archive *archive, long to_position, long from_position, size_t size){
    char buffer[512];

    while (size != 0) {
        int err = tar_seek(archive, from_position);

        if (err){
            return err;
        }

        err = tar_read(archive, &buffer, sizeof(buffer));

        if (err) {
            return err;
        }

        err = tar_seek(archive, to_position);

        if (err) {
            return err;
        }

        err = tar_write(archive, &buffer, sizeof(buffer));

        if (err){
            return err;
        }

        to_position += sizeof(buffer);
        from_position += sizeof(buffer);
        size -= sizeof(buffer);
    }

    return 0;
}


/**
 * Move the current entry to the current write position.
 */ 
int tar_move_entry(tar_archive *archive){
    long entry_size   = (archive->entry_end - archive->entry_start);
    long old_position = archive->entry_start;
    long new_position = archive->write_position;

    int err = tar_move_data(archive, new_position, old_position, entry_size);

    if (err) {
        return err;
    }
    
    // The next entry will be written at the end of this one.
    archive->write_position += entry_size;

    return 0;
}

int tar_find_and_delete_files(tar_archive *archive, char *files[], int file_count){
    for (;;) {
        int res = tar_next(archive);
        
        // In case there are no more entries in the archive
        // we wil stop here.
        if (res) {
            return tar_result(res);
        }

        // In case of a match skip to next entry, otherwise move the entry.
        if (tar_match_file_names(archive, files, file_count) == 0){
            int err = tar_move_entry(archive);

            if (err){
                return err;
            }
        }
    }
}

int tar_delete_files(tar_archive *archive, char *files[], int file_count){
    int err;

    if (err = tar_begin_delete(archive, files, file_count)){
        return err;
    }

    if (err = tar_find_and_delete_files(archive, files, file_count)){
        return err;
    }

    if (err = tar_end_delete(archive)){
        return err;
    }
}

/**
 * Delete the specified list of files/directories from the archive.
 * 
 * Returns: 0 on success or -1 if an error occured.
 */
int tar_delete(tar_archive *archive, char *files[], int file_count){
    int err = tar_find_files(archive, files, file_count);
    
    if (err){
        return err;
    }

    if (err = tar_delete_files(archive, files, file_count)){
        tar_set_error(TAR_ERR_DELETE_ARCHIVE,
                      TAR_ERR_DELETE_ARCHIVE_STR);

        return err;
    }

    return 0;
}
