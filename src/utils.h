
#ifndef TAR_UTILS_H
#define TAR_UTILS_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * macros
 */

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define DEFAULT_DIR_MODE 0755

/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>

/* //////////////////////////////////////////////////////////////////////////////////////
 * interfaces
 */

unsigned int oct2uint(char *oct);

long fsize(FILE *file);

long round_up(long value, long divider);

int is_zeroed(const char *ptr, size_t size);

int mkpath(const char *path_name, mode_t mode);

int mkpath_default_mode(const char *path_name);

int int_to_octal_str(int decimal, char *octal, size_t octal_length);

int int_to_str(int value, char *str, size_t str_length);

int file_exists(const char *file_name);

int file_is_directory(const char *file_name);

#endif
