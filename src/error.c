/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "error.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * extern
 */

extern int errno;

/* //////////////////////////////////////////////////////////////////////////////////////
 * globals
 */

static __thread tar_error error;

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

void _tar_set_system_error(const char *filename, int line, const char *function){

    error.line = line;
    error.code = errno;
    
    snprintf(error.filename, sizeof(error.filename), "%s", filename);
    snprintf(error.function, sizeof(error.function), "%s", function);
    snprintf(error.message, sizeof(error.message), "%s", strerror(errno));
}

void _tar_set_error(const char *filename, int line, const char *function,
    int code, const char *message){ 

    error.line = line;
    error.code = code;

    snprintf(error.filename, sizeof(error.filename), "%s", filename);
    snprintf(error.function, sizeof(error.function), "%s", function);
    snprintf(error.message,  sizeof(error.message),  "%s", message);
}

void _tar_assure_error(const char *filename, int line, const char *function, 
    int code, const char *message){

    if (error.code == 0){
        return _tar_set_error(filename, line, function, code, message);    
    }

}

void _tar_set_file_error(const char *filename, int line, const char *function,
    int code, const char *file_name, const char* message){

    error.line = line;
    error.code = code;

    snprintf(error.filename, sizeof(error.filename), "%s", filename);
    snprintf(error.function, sizeof(error.function), "%s", function);
    snprintf(error.message,  sizeof(error.message),  "%s '%s'", message, file_name);
}

void _tar_assure_file_error(const char *filename, int line, const char *function, 
    int code, const char *file_name, const char* message) {

    if (error.code == 0){
        return _tar_set_file_error(filename, line, function, code, file_name, message);
    }
}

/**
 * Prints the string representaion of the last tar error to stdout.
 */
void tar_print_error(){

    printf("smtar: %s \n", error.message);
}

/**
 * Prints all information about the last tar error to stdout,
 * 
 * @note
 *  The information includes the filename and line where the error was set.
 */
void tar_print_dev_error(){
    
    printf(" %s in function '%s':\n", error.filename, error.function);
    printf(" %s:%d: %d %s \n", error.filename, error.line, error.code, error.message);
}

/**
 * Clears the tar error state.
 */
void tar_clear_error(){
    error.line = 0;
    error.code = 0;

    snprintf(error.filename, sizeof(error.filename), "%s", "");
    snprintf(error.function, sizeof(error.function), "%s", "");
    snprintf(error.message,  sizeof(error.message),  "%s", "");
}

/**
 * Returns the last tar error code.
 */
int tar_get_error_code(){
    return error.code;
}

/**
 * Returns the string representaion of the last tar error.
 */
char* tar_get_error_str(){
    return error.message;
}

/**
 * Returns the last system error code.
 */
int tar_system_errno() {
    return errno;
}

/**
 * Returns the last system error description.
 */
char* tar_system_strerror(){
    return strerror(errno);
}
