
#ifndef TAR_H
#define TAR_H

/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "error.h"
#include "utils.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * types
 */

typedef struct {
	char name[100];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char chksum[8];
	char typeflag;
	char linkname[100];
	char magic[6];
	char version[2];
	char uname[32];
	char gname[32];
	char devmajor[8];
	char devminor[8];
	char prefix[155];
    char padding[12];
} tar_header;


typedef struct {
    FILE *file;

	// The archive`s file name.
	char name[256]; 

	// The current postion in the file.
    long position;

	// The position of the current header(and start of the current file).
    long entry_start;

	// The position indicating the end of the current file.
	long entry_end;

	// The position where we can write.
	long write_position;

} tar_archive;
    
enum {
  TAR_REGULAR   = '0',
  TAR_DIRECTORY = '5'
};

enum {
	TAR_CREATE	= 0,
	TAR_READ	= 1,
	TAR_UPDATE  = 3,
};

enum {
	TAR_SUCCESS =  0,
	TAR_ERROR   = -1,
	TAR_EOF     =  1,
};

/* //////////////////////////////////////////////////////////////////////////////////////
 * interfaces
 */

tar_archive *tar_open(const char *path, int mode);

int tar_close(tar_archive *archive);

int tar_list(tar_archive *archive, char **files, int file_count);

int tar_extract(tar_archive *archive, char **files, int file_count);

int tar_append(tar_archive *archive, char **files, int file_count);

int tar_delete(tar_archive *archive, char *files[], int file_count);

int tar_find_file(tar_archive *archive, const char *path);

int tar_find_files(tar_archive *archive, char *files[], int file_count);

int tar_check(tar_archive *archive);

#endif
