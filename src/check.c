/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "common.h"
#include "utils.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

/**
 * Checks if the header at the current position is valid.
 * 
 * Returns: 1 on success otherwise returns 0.
 */
int tar_is_valid_header(tar_archive *archive){
    tar_header header;
    int err = tar_read_header(archive, &header);

    if (err){
        return 0;
    }

    return (tar_checksum(&header) == oct2uint(header.chksum));
}

/**
 * Determines whether the tar files is empty or not. 
 * 
 * Returns: 1 on success otherwise returns 0.
 */
int tar_is_empty(tar_archive *archive){
    return (fsize(archive->file) == 0);
}

/**
 * Searches for a valid header in the archive.
 *
 * Returns: 1 on success otherwise returns 0.
 */
int tar_find_valid_header(tar_archive *archive, int header_count){

    for (int idx = 0; idx < header_count; idx++){
        int res = tar_next(archive);

        // In case there are no more entries in the archive
        // or we have an error this is not archive.
        if (res) {
            return 0;
        }

        // In case the current header is valid
        // we will stop here.
        if (tar_is_valid_header(archive)) {
            return 1;
        }
    }

    return 0;
}

/**
 * Determines whether the archive is a tar file or not.
 * 
 * Returns: 1 on success otherwise returns 0.
 */
int tar_is_archive(tar_archive *archive){

    // In case we have an empty file, consider it 
    // a valid archive.
    if (tar_is_empty(archive)) {
        return 1;
    }

    // Check the first two headers for now, if at least 
    // one is valid this is a tar file.
    return tar_find_valid_header(archive, 2);
}

/**
 * Checks whether the specified archive is a tar file. 
 * 
 * @note It does not guaranty that every entry in the archive is valid.
 * Returns: 1 on success otherwise returns -1.
 */
int tar_check(tar_archive *archive){

    if (!tar_is_archive(archive)){
         tar_set_error(TAR_ERR_CORRUPTED_ARCHIVE,
                       TAR_ERR_CORRUPTED_ARCHIVE_STR);

        return -1;
    }

    return 0;
}
