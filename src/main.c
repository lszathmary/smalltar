/* //////////////////////////////////////////////////////////////////////////////////////
 * includes
 */

#include "mode.h"
#include "arguments.h"

/* //////////////////////////////////////////////////////////////////////////////////////
 * implementation
 */

int main(int argc, char *argv[]){

    tar_arguments args;
    tar_arguments_parse(&args, argc, argv);
    
    switch ((args.mode))
    {
        case 't':
            return tar_mode_list(args.archive, args.files, args.file_count);

        case 'x':
            return tar_mode_extract(args.archive, args.files, args.file_count);

        case 'c':
            return tar_mode_create(args.archive, args.files, args.file_count);

        case 'r':
            return tar_mode_append(args.archive, args.files, args.file_count);

        case 'd':
            return tar_mode_delete(args.archive, args.files, args.file_count);
        
        default:
            break;
    }
    
    return 0;
}
